---
title: Help give Remote.com a new look
date: "2019-03-07"
description: We’re rebuilding Remote.com from scratch. We want you to also feel like it’s something new and different. To that end, we’ve decided to do a complete rebranding of Remote.com. We will be doing this in the open, and we need your help.
image: "u3.svg"
socialImage: "u3.png"
author: Job
authorLink: https://www.twitter.com/Jobvo
category: "company"
---

[We’re rebuilding Remote.com from scratch][post], and we’ve decided to do a
complete rebranding to make it feel sparkly and new. We're doing this in the open, and we'd love your help.

[post]: https://blog.remote.com/we-re-rebuilding-remote-with-a-new-team/

## Process of rebranding

We’re looking to build a new image of Remote.com, which includes color, tone,
and even a new logo. These choices will influence how the rest of Remote.com will
look, but they are merely a guideline for the interface, not a template.

There are several components leading up to our releasing the new branding.
Everything we do related to the rebrand will be open to contributions(!) and discussion.
Below is a high level overview of the components.

1. Market research
1. Define the intention behind the brand, as a [design persona](https://alistapart.com/article/crafting-a-design-persona)
1. Create a few design directions for the brand identity, in the form of naming, logo, tagline, etc.
1. Choose one direction and create brand guidelines, including colors, typography, and illustration/photographic styles

We’re working with [Pedro Moreira da Silva](https://twitter.com/PedroMScom)
(an amazing designer and friend), and everyone who wants to participate. All our
work and communication takes place in [our branding project][gitlab]. It’s
public, so you can easily view and contribute there. Even the
[meeting agendas](https://docs.google.com/document/d/1jy4NWOj6Zi0GoweihE82tandeQ379TOlh0nbp-K2QtQ/edit?usp=sharing)
are public, and we intend to share the recordings of future meetings on YouTube.

To help us in narrowing our design direction, we'll send out a [survey][survey] to contributors
who want to have a say in the creation of impactful designs.

Finally, our brand guidelines and design persona will be made available in our
[handbook](https://handbook.remote.com/) or on our website.

## How you can help

Rebranding is a large undertaking, and we'd love your help! Whether you run a
company, are looking for a job, or appreciate creativity and design, your feedback is what we want.

To help rebrand Remote.com:

1. [Sign up to take our survey][survey]: We'll send you an email once we have initial design directions and will ask for your opinion. We'll share the aggregated results in the near future.
1. Contribute to our [branding project][gitlab]. We'd appreciate it if you shared your opinion, designs, or whatever you want. All feedback is welcome!

We'll release updates on this blog when we have them. Ultimately, we hope to
create something that you’ll enjoy, so your input is very important to the success of the rebranding.

[survey]: https://goo.gl/forms/W8hcmvxwWVDkBGpg1
[gitlab]: https://gitlab.com/remote-com/branding/issues
