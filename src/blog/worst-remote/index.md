---
title: This is the worst Remote we'll ever ship
date: "2019-08-28"
description: This is the new Remote! It's the worst!
image: "maintenance.svg"
socialImage: "maintenance.png"
author: Job
authorLink: https://www.twitter.com/Jobvo
category: "company"
---

_**TL;DR** This is the new Remote! It's fast and simple. We'd love to hear what you think!_


Welcome to the new Remote! It's the worst version you'll ever see!

The new Remote does a whole lot less than the previous version (which wasn't ours), and a lot less than we want it to do. Here are some things we didn't add yet:

- Salary information
- Multiple location support
- More details on how companies work remotely
- Inline editing of fields
- Multiple user per company support
- Elaborate job alerts
- Reviews
- Special pages for special jobs and companies
- Better support for tags
- implement our new branding and design everywhere (e.g. this blog)
- Our community forum
- A complete absence of bugs

## Why we're already live

The [Remote.com](http://remote.com) of yesterday was not built by us. We were not able to iterate on it quickly. It wasn't a solid basis for our ambitions. To be able to move forward, [we knew we had to start from scratch](https://blog.remote.com/why-we-re-rewriting-remote-com/).

It's very hard to start from scratch and not spend an immense amount of time replicating everything that came before.
It's also very hard to release something. Generally, the earlier you release a product, the better. It allows you to iterate based on feedback from your users, rather than your own hunches.

We knew this, and it still took us a while to get where we are today.
The current Remote is missing some nice features of the old,
but it has brought us the ability to quickly iterate, and be able to control
quality and provide support much _much_ better.

## Our ambition

We believe that the domain [remote.com](http://remote.com) should live up to its name.
If you want to work remotely, hire someone for your distributed company, are looking for resources,
articles or anything else related to remote work, **Remote** should be the place to get it.

More people should be able to work remotely. In everything we do, we hope to contribute to that.

We have big plans.

## The new Remote

Today we're launching our new branding, and our new website.
It's built on an incredibly solid foundation and will allow us to easily handle large amounts of traffic, while quickly making changes.

Search is really fast and powerful.
You can easily filter for all sorts of things, including how remote companies are, and from where a job can be done.
On top of that, we're very proud of our new look and branding!

Over the coming weeks, we'll be writing extensively about our decisions,
plans and new releases. If you want to follow along, [just sign up](https://remote.com/registrations/new) and you'll get our newsletter in your
very own mailbox.

We'd love to hear what you think. We know we have a million things to address, but we've only written down about
five of them. Tell us what you like, and what not. We'll let you know what we're doing.

If you want to post a job today, you can [post the first one for free](https://remote.com), and use the
code REMOTELAUNCH for a big chunk off the final price for any subsequent jobs.
