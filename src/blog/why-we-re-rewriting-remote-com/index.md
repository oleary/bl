---
title: Why we’re rewriting Remote.com
date: "2019-03-13"
description: In the world of programming, a rewrite is often seen as a very bad thing. It is very is to underestimate the complexity of an existing application, and to overestimate your ability to delivery quickly. Yet here we are, rewriting Remote.com. Why?
image: "rewrite.svg"
socialImage: "rewrite.png"
author: Job
authorLink: https://www.twitter.com/Jobvo
category: "company"
---

In the world of programming, a _rewrite_ is often seen as a very bad thing.
It is very easy to underestimate the complexity of an existing application, and to overestimate your ability to delivery quickly.
Yet here we are, rewriting Remote.com. Why?

When [Marcelo][marcelo] and [I][job] started to evaluate the current codebase of Remote.com,
we initially weren’t planning on rewriting at all. We were going to build on the existing code, and maybe remove a part here or there.

The Remote.com that you see today is built on a Python and MongoDB backend, with an AngularJS 1 frontend.
[Marcelo][marcelo] has very extensive experience with large Python backends, and knew that our iteration speed would not be as high as we’d hoped for.
We also knew that Angular 1 isn’t doing speed or maintainability any favors, but that migrating to a newer Angular or alternative framework would be in rewrite-territory.
After some more digging, we discovered some challenges with the architecture of how data is shipped between backend and frontend.
For example: You might notice today that loading search results is somewhat slow. That is because it’s a single, giant payload that is loaded on each request.

## Moving forward

A great product is continuously changing, driven by the vision of its creators and the feedback from its customers.
A product you can change faster will generally do better.

We were not confident we would be able to change Remote.com as frequently on the old code base.
Plus, we were not intending to rebuild every last feature.
This means that we can start from scratch.
The only thing we really have to keep is the underlying data. And we were confident we could migrate that.

With the decision made to rewrite, it was obvious what we were going to build upon. Modern, yet reliable frameworks that would brings benefits missing today:
quick to build, extremely fast for the end-user, and easy to iterate on.

The new Remote.com runs on:

- Backend: Elixir/Phoenix, Elasticsearch, PostgreSQL, Redis
- Frontend: React, Sass

For more on what we use, follow this blog, and check out our [stackshare profile](https://stackshare.io/remoteengineering/remote-com).

## I'm currently using Remote.com. What should I do?

Let us know below how we can improve it! Your old data will be migrated to the new Remote when we launch, later this year.

[marcelo]: https://twitter.com/marcelo_lebre
[job]: https://www.twitter.com/Jobvo
