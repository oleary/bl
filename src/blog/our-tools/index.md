---
title: These are the tools we're using to build the new Remote.com
date: "2019-03-29"
description: Fundamental to great teams working fast is great communication. Tools are just a way to make that easier or possible, not a solution for a poorly communicating or slow team. At Remote.com we try to avoid adopting too many tools. These are the tools we do use every day.
image: "tools.svg"
socialImage: "tools.png"
author: Job
authorLink: https://www.twitter.com/Jobvo
category: "company"
---

Fundamental to great teams working fast is great communication. Tools are just a way to make that easier or possible, not a solution for a poorly communicating or slow team. At Remote.com we try to avoid adopting too many tools. These are the tools we do use every day.

## Slack 💬

[Slack](https://www.slack.com) is a great chat tool. It's easy to overuse, and although Slack claims to reduce emails, we suspect can easily be damaging to productivity. One great use of Slack is to ingest feeds of data. For instance, any mentions of @remote on Twitter, but also whenever a development build fails, we get a message in Slack.

Slack has a free offering, which is good enough for small teams. To be able to have single-channel guests, you need to have a paid subscription.

## Slite 📘

[Slite](https://www.slite.com) hopes to solve the problem of shared documentation. At Remote.com, we've relied on static applications such as the one you're reading from now. The problem is that the barrier to start editing content of a static is quite high. Slite makes it easy to work together on content, but also easy to manage many different types of documents.

We're just starting to use this, but it seems really promising. We're using Slite today to write permanent documentation (both internal, and things that in the future become external).

## GSuite 📬

We use Google's business suite for email, documents, calendar, etc. It's very solid, albeit not very exciting. It's also very affordable at about \$5 per user.

## Sketch ✏️

For all mockups, both low fidelity, as well as pixel-perfect designs, we use [Sketch](https://www.sketch.com). Sketch has a great sharing feature, Sketch Cloud, which we use to share the designs in the team. We have a single master file in which we do all the work.

For a while we used Abstract to version control Sketch files. But given it's mostly just me editing the Sketch file, Abstract added unnecessary complexity.

## Zoom 📞

[Zoom](https://www.zoom.us) is the best video call software there is. I've tried them all. It scales easily\_, \_is very reliable, doesn't take up all your computer's resources. It's no surprise they've just filed for IPO.

You can use Zoom for free, but your meeting length is limited. Plans beyond the free tier start at \$14.99 per host, which is what we're paying now happily.

## GitLab 🛠

[GitLab](https://about.gitlab.com) is a really amazing tool (note that [I might be a little biased](https://www.linkedin.com/in/jobvo/)). We use it to collaborate on code, but also to run our continuous integration and deploy to our staging environment (we don't have a production environment yet!).

Besides that, all our ideas, and specifications go straight into GitLab. It has very powerful project management features.

We have a public GitLab group where we publish all our open source work here: gitlab.com/remote-com.

## Software engineering 💻

In a future blog post, we'll go into our tech stack and the tools we're using. For now, you can checkout our [stackshare page](https://stackshare.io/remoteengineering/remote-com).

What tools are you using?
