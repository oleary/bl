---
title: How companies review your application, and why you don't hear anything back
date: "2019-04-04"
description: You found your dream position, poured all your time and energy into an amazing cover letter.. and you hear nothing back at all from the company you applied to. Sure, your CV wasn't a perfect match, but you more than compensate for that - which you all explained in your cover letter. Then why didn't you get a response? Or if you got one, why was a it such an impersonal one, without any feedback?
image: "review.svg"
socialImage: "review.png"
author: Job
authorLink: https://www.twitter.com/Jobvo
category: "guides"
---

You found your dream position, poured all your time and energy into an amazing cover letter.. and you hear nothing back at all from the company you applied to. Sure, your CV wasn't a perfect match, but you more than compensate for that - which you all explained in your cover letter. Then why didn't you get a response? Or if you got one, why was a it such an impersonal one, without any feedback?

## How companies review applications

Most organizations use an Applicant Tracking System (ATS). Examples are Workable, Lever and Greenhouse. When you send in your application, be it to an email address, or a form on a website, it ends up in one of these applications.

Depending on the size of the company, they might have a team of recruiters or their own managers reviewing incoming applications here. For companies that want to grow fast, and companies that have many applicants (because they are popular), they could be receiving tens or even hundreds of applicants for any given position. For example, for one hire I've made last year, I rejected more than 300 people before hiring someone.

To manage so many applications, the recruiter has to evaluate profiles quickly. This means that applicants that do not match the requirements are usually rejected quickly. Your cover letter might not even be seen. And because the recruiter might have to review hundreds of applications, they will probably use a bulk-reject function[ like this one in Lever](https://help.lever.co/hc/en-us/articles/360022879511-How-do-I-reject-or-archive-candidates-in-bulk-).

This is probably what happens with your application if you get rejected, or don't hear anything. You might be the best fit, but if a quick scan of your profile doesn't reveal that, you might be rejected without much consideration.

## Companies optimize for scale

An organization that wants to scale up quick, that is - hire many people really quickly, will closely monitor how many people are hired, and how quickly. This is why ATSs market their reporting functions heavily, [see this page by Greenhouse for example](https://www.greenhouse.io/recruiting).

To hire people fast, they usually create strict job descriptions and might have internal rules that rank people higher or lower. For example, companies might prefer applicants with a university degree over applicants that don't. Some companies avoid applicants that tend to job-hop, others will only review applicants that come from other notable companies.

I'll refrain from going into [unconscious bias](https://en.wikipedia.org/wiki/Implicit_stereotype) here, but unfortunately this plays a role here as well.

The recruiter that does most of the first-stage filtering and selection will not have a deep understanding of the role and requirements. They often hire for several roles, and it's impossible for them to understand that someone that does experience X might fit really well for role Y as well. They might select candidates on "do they have experience Y".

A recruiter that has many applicants will not take any risk with candidates. The next person to interview a candidate is usually a manager or coworker: someone that does not spend all their time hiring. Recruiters are therefore under pressure to only pass through 'safe bets'.

## Bypassing the process

You're convinced you'll make a great fit for a particular position, but your application still got rejected. How do you still make it in?

If you didn't get a reply at all, ask for one! It might be that a company has simply missed your application. If a company doesn't reply to your email, reach out to them on social media or reach out to the recruiter directly if that's possible.

Got a standard rejection? Try to understand why, again by reaching out directly, but usually your best bet is to talk to the people that you'd be working with. You can easily find those people on Linkedin and social media. Reach out with a short message. Be kind and humble.

One thing that I've seen work is to reach out to the person that would be your manager. They have the best understanding of what kind of person would fit and could also tell you why you wouldn't make a good fit. Do be respectful of their time.

Try to be empathetic with the recruiter and company as a whole. It's not nice to get rejected, or not hear anything at all; consider that the people working at that company are likely in a situation that they're not able or discouraged to help you out. The best companies do reply to every applicant, and give feedback. We'll write more about this in the future.

## The best hires are referrals

Above any other way, the best way to get hired at any company is to be referred. Often there are internal incentives for referring a new colleague, plus you get to bypass much of the recruitment process.

So if you're looking for a particular position, ask around in your network first. It might save you a lot of time, and generally your chances are higher.

## All the things I didn't address

You might read this in desperation and don't feel like this was much help. I didn't address any of these things:

- How companies can actually do better
- How we can improve hiring
- No one is replying to me
- I don't have anyone that can refer me
- I can't find a good job
- I don't have experience, but all jobs require experience
- There is a problem with my CV, and I get rejected because of it
- I want to change careers, but I can't find a first position

I'll address each of these over the coming months. Sign up at Remote.com to receive the newsletter that'll let you know when one of these is up.

What is your personal struggle with finding a new job?
