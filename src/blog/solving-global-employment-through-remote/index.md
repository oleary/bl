---
title: Solving global employment through Remote
date: "2019-09-05"
description: Remote is working on solving global employment by allowing companies to employ people locally through Remote
image: "employ.svg"
socialImage: "employ.png"
author: Job
authorLink: https://www.twitter.com/Jobvo
category: "company"
---

It's very difficult to employ someone in another country. This is becoming an issue, as more companies hire people remotely. Today, these people are typically hired as contractors. They miss out on benefits, ease of local employment, and other advantages associated with a full employment. At the same time, companies are often noncompliant with local laws by hiring full-time staff as contractors / freelancers. In addition to struggling to attract and retain talent, who are not looking to spend significant time managing their own entities.

**Remote is working on solving global employment by allowing companies to employ people *locally* through Remote**. That is, full employment local to each and every employee, but a single invoice for the employer.
Remote manages payroll, benefits, and compliance, according to the standards of the employer.

If this sounds interesting, read more below [or sign up here](https://remote.com/employ) to be among the first companies to be able to use this.

## Why solve employment

We made it our mission to create more remote jobs. Talent is distributed evenly across the world, yet great companies are not. Remote work makes it possible for nearly everyone to work with nearly everyone else. Plus, it has great and far-reaching advantages for both employees and employers, something which has been written about at length.

However, there are many challenges with remote work. In talking with companies of different sizes, we realized that:

1. **Good:** Companies that dedicate themselves to work remotely always make it work
2. **Good**: Tools to better communicate, document, and collaborate remotely are widely available, and generally good to very good
3. **Not so good**: *Finding* great people when hiring remotely is quite hard, but easier than locally
4. **Bad**: *Employing* great people is either unsolved, or solved for only a handful of countries through significant investment

We're making it possible for companies of any size to hire anyone for little cost,
without needing to commit long term. This means that if you are founding a startup,
you can find your first employees *anywhere.* To hire them through Remote will be
easier and simpler than any other way of hiring them, for both you, as well as the
employee.

If you're a larger company looking to hire talent and struggling to find it locally, now you can expand your search to anywhere. You can hire the best person on the planet for any particular job.

## No shortcuts

There is no quick or easy solution to employing people everywhere.
To be able to do as we promise above, we're opening entities in nearly every
country in the world, finding local experts to work with (legal, accounting, tax)
and abstracting the local complexities away to something that is digestible by any
employer or employee.

This is very time consuming and hard to do. But it's the one thing we're doing, so
that every other employer doesn't have to. We're committed to doing this carefully and correct,
so no one involved has to worry about their salary, taxes or compliance.

We're slowly rolling out services, country-by-country.
If you have a wish to employ people from a certain country,
please reach out to me directly (job at remote dot com) [or by signing up here](https://remote.com/employ). Your feedback will help us prioritize certain
countries.

## Employ through Remote

Are you building a remote / distributed team and this sounds interesting, please reach out!
[Sign up here](https://remote.com/employ) and we'll ask you some questions to see whether we can help you today,
or soon.

If you rather talk with me directly, you can do so at job at remote dot com.

### What about the new website?

We believe that the domain [remote.com](http://remote.com) should have everything you need. We'll continue to improve the job board, index of remote companies, and add other interesting things that we think are helpful!
We have a long list of things we need to improve, so you'll see those changes coming in over the coming weeks.
Of course, if we can help you employ through us, and we have a job board,
you can imagine there being some sort of integration there!
