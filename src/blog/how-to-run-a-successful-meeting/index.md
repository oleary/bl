---
title: How to run a successful meeting
date: "2019-03-19"
description: A meeting should be short, to the point, and not waste the time of anyone in it. That sounds difficult, but is absolutely achievable. This works for both in-person and remote meetings.
image: "meeting.svg"
socialImage: "meeting.png"
author: Job
authorLink: https://www.twitter.com/Jobvo
category: "guides"
---

A meeting should be short, to the point, and not waste the time of anyone in it. That sounds difficult, but is absolutely achievable. This works for both in-person and remote meetings.

Do this:

## Don't have a meeting

If you can avoid having a meeting, consider not having a meeting.

Something quick can still be disruptive to the work and time of the other
participants. Meetings, even well-ran, are costly.

## Invite necessary participants only

Only invite people to your meeting that need to be there. If you want to satisfy the curiosity of people that don’t need to be there but are interested in what is discussed, you can share the agenda and notes with them (see below).

If your meeting only needs all participants for a part of the time,
do that first, and tell those people to feel free to leave when they are no longer necessary.

A meeting is a tool to achieve a particular goal.
Be careful to not create a culture where being in a meeting means you’re important. It’s not.

Expert move: if you're in a meeting that you don't need to be in and can't contribute to, leave.
Don't disrupt the meeting. Just send a message to your manager / superior and let the person
that organized the meeting know why you left.

## Share an agenda in advance

A link to a Google doc\* is the easiest way to do this. Give everyone edit rights, and tell them you will only discuss things that are on the agenda. Do this at least a day in advance, preferably whenever you send the invite.

Format it like this:

```
1. Job: A brief description of what I like to discuss
	1. Marcelo: A comment I have on Job’s point
	2. Eduardo: Another comment I have on this point
	3. This is a note someone made while this point was being discussed
2. Luís: A different topic I’d like to discuss
```

Your name before your point. If it’s your point, you speak up. Have a clear goal in mind for adding something to the agenda. That can be a FYI, a clear ask or a clear point of discussion.

Everyone that isn’t talking, takes notes.

- You want to have a document that everyone can easily modify. Sending around an attachment, or an email is not a good solution for this.

## Stay on topic

You should only discuss what is on the agenda. Nothing more, nothing less. If someone but the person raising the point is changing the topic / going off-course, shelve the discussion and make it a separate point on the bottom of the agenda.

For example:

> Job: “Let’s discuss what we’ll put on the bagels”
>
> Marcelo: “Can we also have donuts? Other donut-shaped foods maybe?”
>
> Luís: “I prefer cubic foods, actually”
>
> Job: “Let’s make that a point on the bottom of the agenda” \*adds a point to the bottom of the agenda ‘Marcelo: Other food’.

Learning to do this is extremely important for running a good meeting. It’ll make sure you actually treat each point sufficiently, and take notes. This goes for the original topic as the newly added topic.

## Keep it short

Plan your meetings for 30 minutes or less. You’re probably overestimating the time you need for a well-prepared meeting. If you find that you need more time, you can schedule another meeting - and add some time for the next instance.

In my experience, it’s hard to keep everyone engaged for more than 30 minutes. Even within that time, I find that people start to lose attention twenty minutes in.

## Start on time

The meeting starts at the meeting start time. Given you sent an invite in advance, there’s no need to wait for people to arrive if they’re not on time.
Just start the meeting as is scheduled. Don’t wait for people.

If there are points of absent people on the agenda, move them to the bottom of the agenda.

Make it clear that you’ll be starting on time going forward, and be consistent about this. Before you know it, everyone shows up on time.

## End on time

End the meeting early, or exactly when planned. Never later. You only asked for a set amount of people, respect their time by ending a meeting when you’re out of time.

Any points left to discuss should be moved to the next meeting (if there is one), or scheduled into a new meeting with the people that need to hear these.

## Elect yourself as leader

If there's no formal or natural leader in a meeting, take it upon yourself to be that person.

Leading a meeting is a skill that can be learned, get started with:

1. Telling people to start discussing their point once the time of meeting has started.
1. Keep discussions on topic
1. Sticking to the time. Clearly running out of time? Address that _ahead off the end time_, and make a plan for it.
1. Highlight underrepresented voices. People that tend to speak more, or more loudly, do not have better ideas than
   those that do not speak up. As a leader you can level the playing field.

## Conclude and follow up

Once something has been discussed, make it clear what the follow up is, and who is responsible for it.
You should do this right in the agenda, so it's easy to refer back to, e.g.:

```
1. Job: Donuts are great - TODO Job: order donuts
1. Luís: Cubic foods are great too - TODO Luís: schedule meeting with fellow cube-fanatics
```

Once something is done, mark it as such in the same agenda - and provide links where necessary

```
1. Job: Donuts are great - DONE Job: order donuts
1. Luís: Cubic foods are great too - DONE Luís: schedule meeting with fellow cube-fanatics
    1. link to meeting
```

For recurring meetings, start the meeting with a review of TODOs of last meeting.

## What else?

What have I not covered here? What are your meeting annoyances?

Something I didn't cover: Why have a meeting in the first place? What are your rules?
