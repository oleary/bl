---
title: New logo survey outcomes
date: "2019-04-08"
description: We're working on rebuilding and rebranding Remote.com. Recently, we sent out a survey with some initial design directions. This morning, we discussed the outcomes and our plans follow it.
image: "data2.svg"
socialImage: "data2.png"
author: Job
authorLink: https://www.twitter.com/Jobvo
category: "company"
---

We're working on [rebuilding](https://blog.remote.com/we-re-rebuilding-remote-with-a-new-team/) and [rebranding](https://blog.remote.com/help-give-remote-a-new-look/) Remote.com. Recently, we sent out a survey with some initial design directions. This morning, we discussed the outcomes and our plans follow it.

See the recording of the discussion:

<iframe width="560" height="315" src="https://www.youtube-nocookie.com/embed/V37hm9mWuHs" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

[And see the slides with the data here](https://docs.google.com/presentation/d/1sDYQlqjxcu1Xm2-k8ZReLwSlg_M9xssf-bEKfT-DzQ4/edit#slide=id.p)

# What's next

We really appreciate the feedback that we've gotten, and the time many of you have spent filling out our survey.
It helped understand what appealed to you, and what didn't. We're using this to move foward with the next steps,
and let it influence our choices going forward, even though we might not go with the most voted-for option.

We believe the 'wireless' logo does not fit our new identity. Whether we use the 'face' or the 'mention' logo, we think either needs work. The mention logo certainly is recognizable and we really like the thought behind it, but it needs to approach the 'face' logo more, in being more clean and modern.

This week, [Pedro](https://twitter.com/PedroMScom) will be working on trying to do just that: Can we get the mention logo to something modern? If not, we might continue to explore the face logo.
Typography will be handled separately, afterwards.

Thoughts, comments? Let us know below.
