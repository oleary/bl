---
title: We're rebuilding Remote.com with a new team
date: "2019-03-04"
description: When you need to take care of your loved ones, visit the doctor, or just want to go for a run, you should be able to do that. You shouldn’t need to choose between being successful in life and successful in your work.
image: "u2.svg"
socialImage: "u2.png"
author: Job
authorLink: https://www.twitter.com/Jobvo
category: "company"
---

When you need to take care of your loved ones, visit the doctor, or just want to go for a run, you should be able to do that.
You shouldn’t need to choose between being successful in life and successful in your work.
Yet, for most of us, this is not a reality.
We live in the cities where our employer happens to be based, often away from family.
Or we are employed by companies that happen to be based in the same city as our family - better opportunities might lie elsewhere.

Remote work is changing the world by allowing people to work from anywhere, with flexible schedules and better benefits.
Recently, [I][job] wrote about how [remote work made dealing with the birth of my premature-born daughter significantly easier][jobs-blog].
I've made it my personal goal to accelerate the change to flexible work and better work-life balance.

Together with [some amazing people][marcelo], we will be rebuilding Remote.com to aim squarely at
helping more companies work remotely.

[jobs-blog]: https://www.linkedin.com/pulse/im-leaving-gitlab-help-everyone-work-remotely-job-van-der-voort/

## The plan

We’re going to change Remote.com to be the best place to find remote work, recruit amazing people,
identify exemplary companies, and learn how to work as a distributed company.

We're already helping companies work effectively, and (optionally) in a distributed fashion. If you're
interested in that, [talk with us][email].

[email]: mailto:job@remote.com

### Changes to current Remote.com

Everything you see today will be changed. The current site is too slow in function, and too hard to quickly
iterate on. We want to establish a strong basis upon which we can quickly respond to feedback
we get from you, and from everyone else that uses it.

Accessibility is a top priority for us in rewriting Remote.com. Remote work allows people that struggle
to work from an office, to work more effectively. We should make sure anyone can easily find an awesome job
using Remote.com.

Lastly, we want to make sure you can easily manage your data. Putting your information online shouldn't
mean giving up your privacy. We will make it easy to manage who can access your information
and to which degree.

Compared to the current Remote.com, some features will disappear, while other will take their place.
We count on you giving us feedback on what is missing, and what can be removed.

### Roadmap

We want you to help out, and you couldn't do that if you didn't know what our plans were.
Therefore we have created a public roadmap: [roadmap.remote.com][roadmap].

We will keep that up to date, and it also makes for an easy way to see what's TODO / IN PROGRESS or
just shipped. We will also post in this blog of course!

- [roadmap.remote.com][roadmap]

[roadmap]: https://roadmap.remote.com

### Handbook

Remote.com is now a new company, so we created a [handbook] to document how we do things.
It's open source and we'd love for you to contribute. We hope that over time it becomes a good
template on how to run a distributed company.

- [handbook.remote.com][handbook]

[handbook]: https://handbook.remote.com

### Open source

We're not running Remote.com fully open source. We will get into the how and the why in future posts,
but we do intend to open source certain projects. If you want to follow those, simply check our
[GitLab Group remote-com](https://gitlab.com/remote-com).

Some of the things we opened so far:

- [Roadmap repository](https://gitlab.com/remote-com/rm)
- [Handbook repository](https://gitlab.com/remote-com/hb)
- [This very blog repository](https://gitlab.com/remote-com/bl)

### Branding

The look and feel of Remote.com is going to change! More about that soon, but we will be working on
this in the open. Follow along in the branding project:

- [Remote.com branding project](https://gitlab.com/remote-com/branding)

## Please help us!

Building something great is a mix of having an interesting idea, and carefully listening to feedback.
We think we have that interesting idea, but for the feedback we depend on you.

Here’s how you can help:

1. Sign up on Remote.com and you'll receive our newsletter,
   which will let you know what we’re up to. You don’t need to complete your profile if you don’t want that.
   All accounts created on the old Remote.com will be migrated to the new one, of course.
1. Leave a comment below with any feedback you have. What should we build, what shouldn't we build? What is your remote working pro-tip? Coffee, tea or something else?
1. If you run a company and want to collaborate (we help you work remotely, or maybe you just tell us what we should build),
   get in touch directly with me at job@remote.com.

## Follow us

You can also find us on Twitter at [@remote](https://www.twitter.com/remote) and on [Linkedin](https://www.linkedin.com/company/remote.com).

If you want to know who’s running Remote.com going forward, you can follow our new CTO, Marcelo on Twitter [@marcelo_lebre][marcelo] and me [@Jobvo][job].

[job]: https://twitter.com/jobvo
[marcelo]: https://twitter.com/marcelo_lebre
