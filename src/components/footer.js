import React from "react"
import styled from "styled-components"
import theme from "styled-theming"
import { rhythm } from "../utils/typography"

import { LinkColor } from "./darkMode"

const FooterColor = theme("mode", {
  light: "#ababab",
  dark: "#fff",
})

const FooterContainer = styled.footer`
  position: relative;
  bottom: 0;
  left: 0;
  width: 100%;
  margin-top: 50px;
  color: ${FooterColor};
`

const FooterLink = styled.a`
  color: ${LinkColor};
`

const InnerFooter = styled.section`
  max-width: ${rhythm(26)};
  margin: 0 auto;
  padding: 20px 0;
  display: flex;
  align-items: center;
  justify-content: space-between;

  @media screen and (max-width: 600px) {
    flex-direction: column;
    align-items: flex-start;
  }
`

const DarkLightModeColor = theme("mode", {
  light: "#FF4A5A",
  dark: "#fff",
})

const DarkLightModeBorder = theme("mode", {
  light: "1px solid #FF4A5A",
  dark: "1px solid #fff",
})

const DarkLightModeBtn = styled.button`
  padding: 8px 10px;
  border-radius: 4px;
  background-image: none;
  background-color: transparent;
  color: ${DarkLightModeColor};
  box-shadow: none;
  border: ${DarkLightModeBorder};
  cursor: pointer;
  transition: all 0.15s;

  &:hover {
    color: grey;
    border-color: grey;
  }

  @media screen and (max-width: 600px) {
    margin-top: 20px;
  }
`

class Footer extends React.Component {
  render() {
    return (
      <FooterContainer>
        <InnerFooter>
          <div>
            © {new Date().getFullYear()}, Built with
            {` `}❤ by the{" "}
            <FooterLink href="https://remote.com">Remote.com</FooterLink> team
          </div>
          <DarkLightModeBtn type="button" onClick={this.props.toggleMode}>
            toggle dark mode
          </DarkLightModeBtn>
        </InnerFooter>
      </FooterContainer>
    )
  }
}

export default Footer
