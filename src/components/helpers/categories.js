import _ from "lodash"

export function getAllCategories(posts) {
  let categories = []
  // Iterate through each post, putting all found categories into `categories`
  _.each(posts, edge => {
    if (_.get(edge, "node.frontmatter.category")) {
      categories = categories.concat(edge.node.frontmatter.category)
    }
  })

  // Eliminate duplicate categories
  return _.uniq(categories) || []
}
