import React, { Fragment } from "react"
import styled from "styled-components"
import theme from "styled-theming"
import { Link } from "gatsby"
import { getDarkModeCookieValue } from "../components/helpers/cookies"
import { LinkColor } from "../components/darkMode"

const LineBackground = theme("mode", {
  light: "#191b20",
  dark: "#fff",
})

const CategoryContainer = styled.section`
  display: flex;
  align-items: center;
  justify-content: center;
  flex-wrap: wrap;
  margin: 50px 0;

  @media screen and (max-width: 600px) {
    margin: 30px 0;
  }
`

const CategoryLink = styled(Link)`
  text-transform: capitalize;
  font-size: 28px;
  font-weight: 700;
  text-decoration: none;
  color: ${LinkColor};

  &.isActive,
  &:hover {
    text-decoration: underline;
  }

  @media screen and (max-width: 600px) {
    font-size: 23px;
  }
`

const Line = styled.span`
  font-size: 22px;
  color: ${LineBackground};
`

class CategoryList extends React.Component {
  componentDidMount() {
    this.setState({ mode: getDarkModeCookieValue() })
  }

  shouldBeActive = category => {
    if (typeof window !== "undefined") {
      return window.location.pathname.includes(category)
    }
  }

  renderCategories = () => {
    const { categories } = this.props
    const sortedCategories = (categories || []).sort()

    return sortedCategories.map((category, index) => {
      const isActive = this.shouldBeActive(category)

      return (
        <Fragment>
          <CategoryLink
            key={category}
            to={`/${category}`}
            className={isActive ? "isActive" : ""}
          >
            {category}
          </CategoryLink>
          <Line>{index !== categories.length - 1 && `\u00A0|\u00A0`}</Line>
        </Fragment>
      )
    })
  }

  render() {
    return <CategoryContainer>{this.renderCategories()}</CategoryContainer>
  }
}

export default CategoryList
