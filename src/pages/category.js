import React from "react"
import styled from "styled-components"
import theme from "styled-theming"
import { Link, graphql } from "gatsby"

import Layout from "../components/layout"
import SEO from "../components/seo"
import { rhythm } from "../utils/typography"
import { LinkColor, ContentColor } from "../components/darkMode"
import { getDarkModeCookieValue } from "../components/helpers/cookies"
import CategoryList from "./categoryList"
import { getAllCategories } from "../components/helpers/categories"

const PostBackground = theme("mode", {
  light: "#fff",
  dark: "#191b20",
})

const PostTextColor = theme("mode", {
  light: "#282c34",
  dark: "#fff",
})

const PostShadowColor = theme("mode", {
  light: "rgba(0, 0, 0, 0.17)",
  dark: "rgba(0, 0, 0, 0.47)",
})

const Post = styled.article`
  background: ${PostBackground};
  box-shadow: 0 2px 6px 0 ${PostShadowColor};
  border-radius: 8px;
  padding: 35px;
  padding-top: 0;
  max-width: 570px;
  margin: 0 auto 30px;
  text-align: center;

  @media screen and (max-width: 475px) {
    padding: 20px;
  }
`

const PostTitle = styled.h2`
  font-weight: 700;
  font-size: 30px;
  margin: ${rhythm(1 / 4)} 0 0;
  color: ${LinkColor};
  a {
    text-decoration: none;
  }
`

const PostInfo = styled.p`
  color: ${ContentColor};
  opacity: 0.52;
  font-size: 13px;
`

const Author = styled.a`
  color: inherit;
  font-size: 14px;
`

const DateText = styled.small`
  color: inherit;
  display: inline-block;
  margin-bottom: ${rhythm(1)};
  font-size: 100%;
`

const PostText = styled.p`
  color: ${PostTextColor};
  margin: 1em 0;
  text-align: left;
`

const PostImg = styled.img`
  display: block;
  max-width: 300px;
  margin: 0 auto;
  padding: 30px 0px;
  height: auto;

  @media screen and (max-width: 411px) {
    max-width: 100%;
  }
`

const ReadMore = styled(Link)`
  color: ${LinkColor};
`

class TagsIndex extends React.Component {
  constructor(props) {
    super(props)

    this.state = {
      mode: "light",
    }
  }

  componentDidMount() {
    this.setState({ mode: getDarkModeCookieValue() })
  }

  render() {
    const { data } = this.props
    const { mode } = this.state

    const siteTitle = data.site.siteMetadata.title
    const posts = data.general.edges
    const nonFilteredPostCategories = data.categories.edges
    const categories = getAllCategories(nonFilteredPostCategories)

    return (
      <Layout
        location={this.props.location}
        title={siteTitle}
        mode={mode}
        toggleMode={this.toggleMode}
      >
        <SEO
          title="All posts"
          keywords={[`blog`, `remote`, `remote work`, `remote jobs`]}
        />

        <CategoryList posts={posts} categories={categories} />

        {posts.map(({ node }) => {
          const title = node.frontmatter.title || node.fields.slug

          return (
            <Post key={node.fields.slug}>
              {node.frontmatter.image && (
                <PostImg
                  src={require(`../assets/images/${node.frontmatter.image}`)}
                />
              )}
              <PostTitle>
                <Link
                  style={{
                    boxShadow: `none`,
                    color: `inherit`,
                  }}
                  to={node.fields.slug}
                >
                  {title}
                </Link>
              </PostTitle>
              <PostInfo>
                by{" "}
                <Author href={node.frontmatter.authorLink}>
                  {node.frontmatter.author}
                </Author>{" "}
                on <DateText>{node.frontmatter.date}</DateText>
              </PostInfo>
              <PostText
                dangerouslySetInnerHTML={{
                  __html: node.frontmatter.description || node.excerpt,
                }}
              />
              <ReadMore to={node.fields.slug}>Read the whole post</ReadMore>
            </Post>
          )
        })}
      </Layout>
    )
  }
}

export default TagsIndex

export const pageQuery = graphql`
  query($category: String) {
    site {
      siteMetadata {
        title
      }
    }
    categories: allMarkdownRemark {
      edges {
        node {
          frontmatter {
            category
          }
        }
      }
    }
    general: allMarkdownRemark(
      sort: { fields: [frontmatter___date], order: DESC }
      filter: { frontmatter: { category: { in: [$category] } } }
    ) {
      edges {
        node {
          excerpt
          fields {
            slug
          }
          frontmatter {
            date(formatString: "MMMM DD, YYYY")
            title
            description
            image
            author
            authorLink
          }
        }
      }
    }
  }
`
