import Typography from "typography"
import Wordpress2016 from "typography-theme-wordpress-2016"

Wordpress2016.overrideThemeStyles = () => {
  return {
    "a.gatsby-resp-image-link": {
      boxShadow: `none`,
    },
    "h1, h2, h3, h4, h5, h6": {
      FontFamily: `Inter`,
      color: `#252525`,
    },
    a: {
      color: `#2449f9`,
      textDecoration: `underline`,
      boxShadow: `none`,
    },
    "a:hover": {
      textDecoration: `none`,
    },
    p: {
      letterSpacing: `-.013em`,
      marginTop: 0,
      color: `#1b1a1a`,
      lineHeight: `1.7em`,
    },
  }
}

Wordpress2016.bodyFontFamily = ["Inter", "Georgia"]

delete Wordpress2016.googleFonts

const typography = new Typography(Wordpress2016)

// Hot reload typography in development.
if (process.env.NODE_ENV !== `production`) {
  typography.injectStyles()
}

export default typography
export const rhythm = typography.rhythm
export const scale = typography.scale
